package com.example.f1419.lab4_felipemartinez;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private EditText username, password;
    private Button send, photo;
    private ImageView image;

    private FirebaseAuth mAuth;
    private StorageReference storageRef;
    Uri imageUri = null;
    int serial=0;
    int actual = 0;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference myRef = database.getReference("User");

    boolean status = false;

    private static final String TAG = "Auth";
    private static final int IMAGE_CAPTURE = 101;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        storageRef = FirebaseStorage.getInstance().getReference();

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        send = findViewById(R.id.send);
        photo = findViewById(R.id.photo);
        image = findViewById(R.id.image);

        if (! hasCamera () )
            photo.setEnabled( false ) ;
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent ( MediaStore. ACTION_IMAGE_CAPTURE ) ;
                startActivityForResult ( intent , IMAGE_CAPTURE) ;
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (imageUri==null){
                    Toast.makeText(MainActivity.this, "You must take a photo", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (status){

                        uploadPhoto(actual);
                        Toast.makeText(MainActivity.this, "The Image of the user was changed", Toast.LENGTH_SHORT).show();
                        status = !status;
                        return;

                }


                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get Post object and use the values to update the UI
                        //Post post = dataSnapshot.getValue(Post.class);
                        // ...

                        serial = 0;
                        for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                            User user = postSnapshot.getValue(User.class);
                            Toast.makeText(MainActivity.this, "Trae de firebase a: "+user.getUsername(), Toast.LENGTH_SHORT).show();
                            serial = Integer.parseInt(postSnapshot.getKey())+1;
                            if(user.getUsername().equals(username.getText().toString())){
                                actual = serial-1;
                                loginUser(user.getUsername(),user.getPassword());
//                                username.setText(user.getUsername());
//                                password.setText(user.getPassword());
//
                                try {
                                    downloadPhoto(serial-1);
                                } catch (IOException e) {
                                    Toast.makeText(MainActivity.this, "¡Something go wrong sorry!", Toast.LENGTH_SHORT).show();
                                }

                                Toast.makeText(getApplicationContext(), "Loggin in the user "+user.getUsername(), Toast.LENGTH_SHORT).show();

                                Toast.makeText(MainActivity.this, "If you want to change the photo just take another", Toast.LENGTH_SHORT).show();
                                status=!status;

                                return;
                            }
                        }
                        // no lo encontró

                        createUser(username.getText().toString(),password.getText().toString());
                        if (password.getText().toString().length()<6){
                            password.setError("The password must have 6 characters");
                            return;
                        }
                        myRef.removeEventListener(this);





                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w("Nada", "loadPost:onCancelled", databaseError.toException());
                        // ...
                    }
                };
                myRef.addValueEventListener(postListener);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                imageUri = data.getData();
                Toast.makeText(this, imageUri.toString(), Toast.LENGTH_SHORT).show();
                image.setImageURI(null);
                image.setImageURI(imageUri);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, " Image capture cancelled .",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, " Failed to capture image",
                        Toast.LENGTH_LONG).show();
            }
            return;
        }
        Toast.makeText(MainActivity.this, "Something go wrong, ¡keep trying!", Toast.LENGTH_SHORT).show();
    }

    private boolean hasCamera () {
        return ( getPackageManager () . hasSystemFeature (
                PackageManager. FEATURE_CAMERA_ANY ) ) ;
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            Toast.makeText(this, "User already signed in", Toast.LENGTH_SHORT).show();
            return;
        }
        //Toast.makeText(this, "Not yet signed in", Toast.LENGTH_SHORT).show();
        //updateUI(currentUser);
    }

    public void createUser(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            Toast.makeText(MainActivity.this, "Creation success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            guardar();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Creation failed", Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    public void loginUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Login failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    public void uploadPhoto(int serial){
        StorageReference photoRef = storageRef.child("images/"+serial+".jpeg");

        photoRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        Toast.makeText(MainActivity.this, "Image upload success", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Toast.makeText(MainActivity.this, "Image upload fail", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void downloadPhoto(int serial) throws IOException {
        final File localFile = File.createTempFile("images", "jpeg");
        StorageReference photoRef = storageRef.child("images/"+serial+".jpeg");
        photoRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Successfully downloaded data to local file
                        // ...
                        Toast.makeText(MainActivity.this, "Image download success"+localFile.getPath(), Toast.LENGTH_SHORT).show();
                        image.setImageURI(Uri.fromFile(localFile));
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
                Toast.makeText(MainActivity.this, "Image download fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void guardar(){
        myRef.child(String.valueOf(serial)).setValue(new User(username.getText().toString(),password.getText().toString()));
        Toast.makeText(getApplicationContext(), "Creating the user "+username.getText().toString(), Toast.LENGTH_SHORT).show();
        uploadPhoto(serial);
    }

}

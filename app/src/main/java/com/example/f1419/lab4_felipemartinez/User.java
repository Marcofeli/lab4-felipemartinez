package com.example.f1419.lab4_felipemartinez;

import android.net.Uri;

/**
 * Created by f1419 on 4/4/2018.
 */

class User {
    private String username;
    private String password;
    private String image;

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

